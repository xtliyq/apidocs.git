<!DOCTYPE HTML>
<html>
<head>
    <title></title>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8"/>
    <link rel="stylesheet" href="css/style.css">
    <link href="https://cdn.bootcss.com/jquery-jsonview/1.2.3/jquery.jsonview.min.css" rel="stylesheet">
    <style>
        .content > aside, .content > dl, .content > h1, .content > h2, .content > h3, .content > h4, .content > h5, .content > h6, .content > ol, .content > p, .content > table, .content > ul {
            margin-right: auto;
        }

        .content > form {
            padding: 0 28px;
        }

        .response {
            display: none;
        }

        .response pre {
            float: initial;
            width: 100%;
            word-break: break-all;
        }

        .response_throbber {
            background-image: url("css/imgs/throbber.gif");
            width: 128px;
            height: 16px;
            display: block;
            clear: none;
            float: right;
        }

        a,
        a:link,
        a:hover,
        a:active,
        a:visited {
            text-decoration: none;
            color: white;
        }
    </style>
</head>
<body>
<div class="tocify-wrapper">
    <div class="tocify">
        @foreach($routes as $key=> $group)
            <ul class="tocify-header">
                <li class="tocify-item" style="cursor: pointer;">
                    <a title="{{$key}}">{{$key}}</a>
                </li>
                <ul class="tocify-subheader">
                    @foreach($group as $route)
                        <li class="tocify-item" style="cursor: pointer;" onClick="itemClick(this)">
                            <a href="#{{$route['uri']}}/{{$route['title']}}"
                               title="{{$route['title'] ? $route['title'] : $route['uri']}}">
                                {{$route['title'] ? $route['title'] : $route['uri']}}
                                {{--<span>{{$route['uri']}}</span>--}}
                            </a>
                        </li>
                    @endforeach
                </ul>
            </ul>
        @endforeach
    </div>
</div>
<div class="page-wrapper">
    <div class="content">
        @foreach($routes as $group)
            @foreach($group as $route)
                <div name="{{$route['uri']}}/{{$route['title']}}" class="item content">
                    <h2 id="{{$route['uri']}}/{{$route['title']}}">
                        {{$route['title'] ? $route['title'] : $route['uri']}}
                    </h2>
                    @if($route['description'])
                        <p>{{$route['description']}}</p>
                    @endif
                    <h3>HTTP Request</h3>
                    @foreach($route['methods'] as $method)
                        <p>
                            <code>
                                {{$method}} {{$route['uri']}}
                            </code>
                        </p>
                    @endforeach
                    <h3>Param</h3>
                    @if( count($route['parameters']) > 0 )
                        <table>
                            <tr>
                                <th>parameter</th>
                                <th>type</th>
                                <th>Description</th>
                            </tr>
                            {{--@foreach($route['params'] as $param)--}}
                            {{--<tr>--}}
                            {{--<td>{{$param->getVariableName()}}</td>--}}
                            {{--<td>{{$param->getType()}}</td>--}}
                            {{--<td>{{$param->getDescription()}}</td>--}}
                            {{--</tr>--}}
                            {{--@endforeach--}}
                            @foreach($route['parameters'] as $param)
                                <tr>
                                    <td>{{$param['key']}}</td>
                                    <td>{{$param['type']}}</td>
                                    <td>{{$param['description']}}</td>
                                </tr>
                            @endforeach
                        </table>
                    @else
                        <dl>无</dl>
                    @endif


                    <h3>Test</h3>
                    <form action="{{$url}}/{{$route['uri']}}" method="post">
                        <dl>
                            <label>请求方式</label>
                            <select name="_method">
                                @foreach($route['methods'] as $method)
                                    <option>{{$method}}</option>
                                @endforeach
                            </select>
                        </dl>
                        @if(strpos($route['middleware'],'auth') !==false)
                            <dl>
                                <label>Authorization</label>
                                <textarea name="Authorization" placeholder="请输入token值"></textarea>
                                <span>token值</span>
                            </dl>
                        @endif

                        {{--@foreach($route['params'] as $param)--}}
                        {{--<dl>--}}
                        {{--<label>{{$param->getVariableName()}}</label>--}}
                        {{--<input name="{{$param->getVariableName()}}" placeholder="{{$param->getDescription()}}">--}}
                        {{--</dl>--}}
                        {{--@endforeach--}}
                        @foreach($route['parameters'] as $param)
                            <dl>
                                <label>{{$param['key']}}</label>
                                <input name="{{$param['key']}}" placeholder="{{$param['description']}}">
                                <span>{{$param['description']}}</span>
                            </dl>
                        @endforeach

                        <dl>
                            <button type="submit">try</button>

                            <span class="response_throbber" style="display: none"></span>
                        </dl>

                        <div class="response">
                            <h3>Request URL</h3>
                            <pre class="response-url"></pre>

                            <h3>Request Headers</h3>
                            <pre class="response-headers"></pre>

                            <h3>Response Body</h3>
                            <pre class="response-json"></pre>

                            <h3>Response Code</h3>
                            <pre class="response-code"></pre>

                            <h3>Response Header</h3>
                            <pre class="response-res-headers"></pre>
                        </div>
                    </form>
                </div>
            @endforeach
        @endforeach
    </div>
</div>
<script src="https://cdn.bootcss.com/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdn.bootcss.com/jquery-jsonview/1.2.3/jquery.jsonview.js"></script>
<script>
    function itemClick(elem) {
        $(".tocify-subheader").find('.tocify-focus').removeClass('tocify-focus');
        $(elem).addClass('tocify-focus');
    }

    $(document).ready(function () {
        var $win = $(window);
        var winHeight = $win.height();

        //滚动处理
        window.onscroll = function (ev) {
            $(".page-wrapper").find('.item').each(function (index, elem) {
                var $elem = $(elem)
                var itemOffsetTop = $elem.offset().top;
                var itemOuterHeight = $elem.outerHeight();
                var winScrollTop = $win.scrollTop();
                if (!(winScrollTop > itemOffsetTop + itemOuterHeight) && !(winScrollTop < itemOffsetTop - winHeight)) {
                    var name = $elem.attr('name')
                    $(".tocify-subheader").find('.tocify-focus').removeClass('tocify-focus');
                    $("[href='#" + name + "']").parent().addClass('tocify-focus');
                    $("[href='#" + name + "']").parent().parent().css('display', 'block');
                    return false;
                }
            });
        }

        $(".tocify-header>.tocify-item").on('click', function () {
            console.log(this);
            $sub = $(this).parent().find(".tocify-subheader");
            if ($sub.css('display') == 'block') {
                $sub.css('display', 'none');
            } else {
                $sub.css('display', 'block');
                $sub.find('.tocify-item:first a').trigger('click');
            }
        });

        $("button[type='submit']").on('click', function (event) {
            event.preventDefault();
            form = $(this).parent().parent();
            form.find('.response_throbber').show();
            var action = form.attr('action');
            var t = form.serializeArray();
            var data = {};
            $.each(t, function () {
                if (this.value && this.value.length > 0) {
                    data[this.name] = this.value;
                }
            });
            //检查url中是否有需要替换的参数
            reg = /\{[a-zA-Z0-9_\?]+\}/ig;
            var method = data['_method'];
            delete  data['_method'];
            if (reg.test(action)) {
                arr = action.match(reg);
                arr.forEach(function (value) {
                    var key = value.replace(/\{|\}/ig, '');
                    if (!data[key]) {
                        if (key.endsWith('?')) {
                            data[key] = data[key.replace('?', '')];
                            delete data[key.replace('?', '')];
                        } else {
                            alert('请输入参数' + key + '的值')
                            return false;
                        }
                    }
                    action = action.replace(value, data[key]);
                    //删除掉替换过的参数
                    delete data[key];
                })
            }
            if (reg.test(action)) {
                form.find('.response_throbber').hide();
                return false;
            }
            var header = {
                'Accept': 'application/json'
            }

            if (data['Authorization']) {
                header = Object.assign(header, {
                    'Authorization': 'Bearer ' + data['Authorization']
                })
                delete data['Authorization'];
            }

            $.ajax({
                url: action,
                data: data,
                headers: header,
                type: method,
                dataType: 'json',
                // success: function (json) {
                //     form.find('.response').css('display', 'block');
                //     form.find('.response-json').JSONView(json)
                // },
                // error: function (XMLHttpRequest, textStatus, errorThrown) {
                //     console.log(XMLHttpRequest, textStatus, errorThrown)
                //     form.find('.response').css('display', 'block');
                //     form.find('.response-json').JSONView(XMLHttpRequest.responseJSON)
                // },
                complete: function (XMLHttpRequest, textStatus) {
                    form.find('.response').show();
                    //显示地址
                    form.find('.response-url').html(this.url);
                    //显示请求头
                    form.find('.response-headers').JSONView(this.headers);
                    //显示返回内容
                    form.find('.response-json').JSONView(XMLHttpRequest.responseJSON);
                    //显示返回code
                    form.find('.response-code').html(XMLHttpRequest.status);
                    //显示返回header
                    form.find('.response-res-headers').html(XMLHttpRequest.getAllResponseHeaders());

                    form.find('.response_throbber').hide();
                }
            })
        });

        $(".tocify-header>.tocify-item:first").trigger('click');
        $("html,body").animate({scrollTop: 0}, 100);
    });
</script>
</body>

</html>