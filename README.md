# apidocs

#### 项目介绍
根据laravel框架路由及Controller内的代码注释生成api文档并支持调用接口进行调试，支持FormRequest验证的参数读取


#### 使用说明

1、安装扩展
```$xslt
 composer require liyq/apidocs
```

2、发布前端资源文件
```
 php artisan vendor:publish
```

3、生成接口文档的
```
php artisan api:generate --routePrefix="api/*"
```

4、在浏览器中访问[http://你的域名/apidocs/]() 查看接口文档

5、预览
![demo1](example/img/demo1.png)
普通预览

![demo2](example/img/demo2.png)
test效果预览